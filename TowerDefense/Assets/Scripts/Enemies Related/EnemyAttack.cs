using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace Quests.Enemy
{
    public class EnemyAttack : MonoBehaviour
    {
        private EnemyController enemyController;

        private void Start()
        {
            enemyController = GetComponent<EnemyController>();
        }
        public void Attack()
        {
            StartCoroutine(attackCoroutine());
        }
        public void stopAttak()
        {
            StopCoroutine(attackCoroutine());
        }
        IEnumerator attackCoroutine()
        {
            if (enemyController.active)
            {
                enemyController.enemyNavMeshController.target.GetComponent<TowerDestinationStats>().GetDamage(enemyController.enemyStats.attackPoints);
                yield return new WaitForSeconds(enemyController.enemyStats.attackRate);
                StartCoroutine(attackCoroutine());
            }
        }
    }
}